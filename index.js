(function() {
    function normalizeChance(chance) {
        if (chance <= 0 || Number.isNaN(Number(chance))) {
            return 1;
        }

        return chance;
    }

    function getRandomIndex(arr) {
        if (!arr || !arr.length) {
            return null;
        }

        return arr[Math.floor(Math.random() * arr.length)];
    }

    window.Chanceable = function Chanceable(chance) {
        this.chance = normalizeChance(chance);
    }

    Chanceable.prototype.roll = function() {
        return Math.random() < this.chance;
    };

    var actionTypes = {
        ALL_ACTIONS: 'all',
        ONE_ACTION: 'one',
    };

    window.Change = function Change(actions, chance, type) {
        /* inherits Chanceable */
        Chanceable.call(this, chance);

        this.actions = actions;
        this.type = type;
    }

    Change.prototype = Object.create(Chanceable.prototype);
    Change.prototype.constructor = Chanceable;

    Change.prototype.execute = function() {
        if (this.type === actionTypes.ALL_ACTIONS) {
            return this.actions.map((action) => action.getCSS());
        } else if (this.type === actionTypes.ONE_ACTION) {
            return [getRandomIndex(this.actions).getCSS()];
        } else {
            throw Error('Change enum type not recognized.');
        }
    };

    window.Action = function Action(rules, choices) {
        this.rules = rules;
        this.choices = choices;
    }

    Action.prototype.getCSS = function() {
        var css = {
            rules: this.rules,
        };

        var choice = getRandomIndex(this.choices);
        if (choice.roll()) {
            css.value = choice.execute();
        } else {
            css.value = '';
        }

        return css;
    };

    window.Choice = function Choice(value, replacements, chance) {
        /* inherits Chanceable */
        Chanceable.call(this, chance);

        if (typeof value !== 'string') {
            throw new Error('value argument in Choice must be a string.');
        }

        this.value = value;
    }

    Choice.prototype = Object.create(Chanceable.prototype);
    Choice.prototype.constructor = Chanceable;

    Choice.prototype.execute = function() {
        return this.value;
    };

    window.Range = function Range(lower, upper, replacementStr, actionType, round, chance) {
        /* inherits Chanceable */
        Chanceable.call(this, chance);

        if (typeof lower === 'number' &&
            typeof upper === 'number' &&
            lower < upper)
        {
            this.lower = lower;
            this.upper = upper;
        } else {
            throw new Error('invalid range datatypes');
        }

        this.replacementStr = replacementStr;
        this.actionType = actionType;
        this.round = round;
    }

    Range.prototype = Object.create(Chanceable.prototype);
    Range.prototype.constructor = Chanceable;

    Range.prototype.execute = function() {
        var returnStr = '';
        if (this.replacementStr) {
            returnStr = (String)(this.replacementStr);
        }

        var num = this.makeRangeValue(this.lower, this.upper, this.round);

        var regex = /%s/;
        var index = 0;
        while (regex.test(returnStr)) {
            if (this.actionType === actionTypes.ALL_ACTIONS) {
                num = this.makeRangeValue(this.lower, this.upper, this.round);
            }

            returnStr = returnStr.replace(regex, num);

            index++;
        }

        if (!returnStr) {
            returnStr = (String)(num);
        }

        return returnStr;
    }

    function makeRangeValue(lower, upper, round) {
        var num = Math.random() * (upper - lower) + lower;
        if (round) {
            num = Math.floor(num);
        }

        return (String)(num);
    }

    var POSITION_CHANCE = 0.15;
    var COLOR_CHANCE = 0.15;

    var colorshift = 
        new Change(
            // actions
            [
                new Action(
                    // rules
                    'color',
                    // choices
                    [
                        new Range(
                            // lower bound
                            0,
                            // upper bound
                            255,
                            // replacement string
                            'rgb(%s, %s, %s)',
                            // action type
                            actionTypes.ALL_ACTIONS,
                            // round
                            true)
                    ]),
                new Action(
                    // rules
                    'left',
                    // choices
                    [
                        new Range(-0.25, 0.25, '%srem'),
                    ]),
                new Action(
                    // rules
                    'top',
                    // choices
                    [
                        new Range(-0.25, 0.25, '%srem'),
                    ]),
                new Action(
                    // rules
                    'mix-blend-mode',
                    // choices
                    [
                        new Choice('multiply'),
                        new Choice('screen'),
                        new Choice('overlay'),
                        new Choice('darken'),
                        new Choice('lighten'),
                        new Choice('color-dodge'),
                        new Choice('color-burn'),
                        new Choice('hard-light'),
                        new Choice('soft-light'),
                        new Choice('difference'),
                        new Choice('exclusion'),
                        new Choice('hue'),
                        new Choice('saturation'),
                        new Choice('color'),
                        new Choice('luminosity'),
                    ]),
                new Action(
                    // rules
                    ['-webkit-transform', '-moz-transform', '-o-transform', 'transform'],
                    // choices
                    [
                        new Range(0.9, 1.1, 'scale(%s, %s)'),
                    ]),
            ],
            // chance of firing 
            0.15,
            // fire all actions if rolled true, or just one?
            actionTypes.ALL_ACTIONS);

    var changes = [
        colorshift,
    ];
    var actions = [

    ];

    function initChangeModule() {
        processGroup();

        var interval = window.setInterval(processGroup, window.);
    }

    function processGroup() {
        var primaries = document.querySelectorAll('.glitchTextPrimary');
        for (var ii = 0; ii < primaries.length; ii++) {
            var primary = primaries[ii];

            var textElem = null;
            var secondaries = [];
            for (var jj = 0; jj < primary.children.length; jj++) {
                var className = primary.children[jj].className;
                var splitClassName = className.split(','); 
                if (!textElem && .indexOf('glitchText') !== -1) {
                    textElem = primary.children[jj];
                }

                if (splitClassName.indexOf('.glitchTextSecondary') !== -1) {
                    secondaries.push(primary.children[xx]);
                }
            }

            if (!textElem) {
                throw new Error('Could not find .glitchText in primary');
            }

            for (var xx = 0; xx < secondaries.length; xx++) {
                var secondary = secondaries[xx];
                secondary.innerText = textElem.textContent;
                process(secondary);
            }
        }
    }

    function process(node) {
        for (var ii = 0; ii < 0; )
        changes.forEach(function(change) {
            if (change.roll()) {
                var results = change.execute();

                results.forEach(function(result) {
                    if (result.rules.forEach) {
                        result.rules.forEach(function(rule) {
                            $(node).css(rule, result.value);
                        });
                    } else {
                        $(node).css(result.rules, result.value);
                    }
                });
            }
        });
    }

    if (!window.Macro) {
        window.Macro = {};
    }

    Macro.mwAction = {
        handler() {
            return new Action();
        },
    };

    Macro.mwChoice = {
        handler() {
            return new Choice();
        },
    };

    initChangeModule();
}());